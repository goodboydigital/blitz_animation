Math.lerp = function (value1, value2, amount)
{
    amount = amount < 0 ? 0 : amount;
    amount = amount > 1 ? 1 : amount;

    return value1 + (value2 - value1) * amount;
};

import * as PIXI from 'pixi.js';
import manifest from './manifest-v2.json';
import Scene from './Scene';
import gboLoader from './gboLoader';

const loader = new PIXI.Loader();

const type = 'default';

const Resource = PIXI.LoaderResource;

Resource.setExtensionXhrType('gbo', Resource.XHR_RESPONSE_TYPE.BUFFER);

loader.use(gboLoader());

for (const key in manifest.default.image)
{
    const element = manifest.default.image[key];

    loader.add(`assets/${element[type]}`);
}
for (const key in manifest.default.json)
{
    const element = manifest.default.json[key];

    loader.add(`assets/${element[type]}`);
}
loader.add('../ressources.json');
loader.add('assets/models/coin.gbo');
loader.add('assets/models/coin-casino.gbo');
loader.load(init);
console.log(loader);

let scene;

function init()
{
    window.resourcesData = loader.resources;
    scene = new Scene(window.innerWidth, window.innerHeight);
    scene.resize(window.innerWidth, window.innerHeight);
    window.addEventListener('resize', resize);
}
function resize()
{
    console.log(window.innerWidth, window.innerHeight);

    scene.resize(window.innerWidth, window.innerHeight);
}
