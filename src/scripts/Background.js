import * as PIXI from 'pixi.js';
import * as filters from 'pixi-filters';
import Smoke from './Smoke';
export default class Background extends PIXI.Container
{
    constructor(scene, w, h)
    {
        super();
        this.scene = scene;
        this.screenWidth = w;
        this.screenHeight = h;
        this.container = new PIXI.Container();
        this.graphics = new PIXI.Graphics().beginFill(0xFFFFFF).drawRect(0, 0, this.screenWidth, this.screenHeight);
        this.addChild(this.graphics);
        this.graphics.tint = 0;
        this.addChild(this.container);

        this.topGradient = PIXI.Sprite.from('assets/image/sky-gradient.png');
        this.bottomGradient = PIXI.Sprite.from('assets/image/sky-gradient.png');

        this.container.addChild(this.topGradient);
        this.topGradient.scale.x = w / this.topGradient.width;
        this.topGradient.scale.y = h / this.topGradient.height;
        this.topGradient.x = 0; // - this.bottomGradient.height
        this.topGradient.y = 0; // - this.bottomGradient.height
        this.container.addChild(this.bottomGradient);
        this.bottomGradient.scale.x = this.topGradient.scale.x;
        this.bottomGradient.scale.y = -this.topGradient.scale.y;
        this.bottomGradient.x = 0; // - this.bottomGradient.height
        this.bottomGradient.y = 0; // - this.bottomGradient.height

        this.topGradient.anchor.set(0.5);
        this.bottomGradient.anchor.set(0.5);

        this.colorSet = {
            topGradientStandard: 0x97E48C,
            topGradientWarp: 0x00FFF7,
            bottomGradientStandard: 0xED3BFF,
            bottomGradientWarp: 0xFFF300,
            topSmokeStandard: 0xFF00FF,
            topSmokeWarp: 0xD4FF00,
            bottomSmokeStandard: 0x00FFFB,
            bottomSmokeWarp: 0xFF3200,
            centerZoneStandard: 0x000000,
            centerZoneWarp: 0x000000,
            annulusWarp: 0x676ff7,
        };

        this.defaultColorSet = this.colorSet;

        this.smokeList = [];

        for (let index = 0; index < 4; index++)
        {
            const smoke = new Smoke(this);

            smoke.build({
                num: 1,
                width: w,
                height: h,
                type: 'top',
            });
            this.container.addChild(smoke);
            this.smokeList.push(smoke);
        }

        for (let index = 0; index < 4; index++)
        {
            const smoke = new Smoke(this);

            smoke.build({
                num: 2,
                width: w,
                height: h,
                type: 'bottom',
            });
            this.container.addChild(smoke);
            this.smokeList.push(smoke);
        }

        this.blur = PIXI.Sprite.from('assets/image/blurStar.png');
        this.container.addChild(this.blur);
        this.blur.scale.x = w / this.blur.width;
        this.blur.scale.y = h / this.blur.height;
        this.blur.anchor.set(0.5);
        this.blur.scale.set(5);
        this.blur.x = 0;
        this.blur.y = 0;

        // debugger

        // this.topGradient.tint = 0x313984
        this.topGradient.tint = this.colorSet.topGradientStandard;
        // this.bottomGradient.tint = 0xFF00FF
        this.bottomGradient.tint = this.colorSet.bottomGradientStandard;

        this.container.alpha = 0.25;

        this.warping = false;

        this.annulus = PIXI.Sprite.from('assets/image/annulus_blur_20.png');
        this.container.addChild(this.annulus);
        this.annulus.anchor.set(0.5);
        this.annulus.scale.set(0.1);
        this.annulus.x = 0;
        this.annulus.y = 0;
        this.annulus.visible = false;

        this.annulusSpeed = 2;
        // 53FF3B
        // ED3BFF

        // for (let index = 0; index < 10; index++) {
        //     const element = PIXI.Sprite.from('assets/image/star.png')
        //     element.anchor.set(0.5)
        //     element.x = w /2 + Math.random() * 550
        //     element.y = h /2 + Math.random() * 550
        //     element.blendMode = PIXI.BLEND_MODES.ADD
        //     this.addChild(element)
        //     element.alpha = 0.2

        // }
        // top: 0x313984,
        //        // top: 0x4e72c4,
        //        // bottom: 0x313984,
        //        bottom: 0xFF00FF,
        //        front1: 0x8985ff,
        //        blur: 0x00FF00,
        //        additiveSky: 0xFF00FF,
        //        slot: 0xFFFFFF,
        this.sin = 0;

        // setTimeout(() => {
        //     pulse
        // }, timeout);
    }

    update()
    {
        // if (!this.warping) return
        for (const key in this.smokeList)
        {
            this.smokeList[key].update(key);
        }

        this.annulus.scale.set(this.annulus.scale.x += 1 / 60 * this.annulusSpeed);
        this.annulus.alpha = Math.abs(window.innerWidth - (this.annulus.width * 0.5)) * 0.002;

        // console.log(this.annulus.width);
        if (this.annulus.width * 0.5 > window.innerWidth)
        {
            this.annulus.scale.set(0.1);
            this.annulus.alpha = 1;
            this.annulus.visible = !this.isIdle;
        }

        return;
        this.sin += 0.05;
        const n = (Math.sin(this.sin) * 0.5 + 0.5);
        const n2 = (Math.cos(this.sin) * 0.5 + 0.5);

        this.blur.scale.y = (this.blur.startScale.y * 0.25) * n2 + this.blur.startScale.y * 0.75;
        this.container.alpha = n * 0.25 + 0.25;
    }

    setWarpEffect(idle)
    {
        this.isIdle = idle;
        if (!idle)
        {
            this.warpStart();
        }
        else
        {
            this.warpEnd();
        }
    }

    resetColors()
    {
        console.log('reset');
        this.colorSet = this.defaultColorSet;
    }

    warpStart()
    {
        for (const key in this.smokeList)
        {
            this.smokeList[key].warpping();
        }
        this.container.alpha = 0.5;
        this.addColorTween(this.topGradient, this.topGradient.tint, this.colorSet.topGradientWarp, 1);
        this.addColorTween(this.bottomGradient, this.bottomGradient.tint, this.colorSet.bottomGradientWarp, 1);
        for (const key in this.smokeList)
        {
            this.smokeList[key].hide();
        }
        this.blur.tint = this.colorSet.centerZoneWarp;
        this.annulus.tint = this.colorSet.annulusWarp;
    }

    warpEnd()
    {
        for (const key in this.smokeList)
        {
            this.smokeList[key].reset();
        }
        this.container.alpha = 0.25;
        this.addColorTween(this.topGradient, this.topGradient.tint, this.colorSet.topGradientStandard, 1);
        this.addColorTween(this.bottomGradient, this.bottomGradient.tint, this.colorSet.bottomGradientStandard, 1);
        for (const key in this.smokeList)
        {
            this.smokeList[key].show();
        }
        this.blur.tint = this.colorSet.centerZoneStandard;
    }

    killColorTween(target)
    {
        for (let i = this.tweenList.length - 1; i >= 0; i--)
        {
            if (this.tweenList[i].target == target)
            {
                this.tweenList[i].tween.kill();
                this.tweenList.splice(i, 1);
            }
        }
    }

    addColorTween(target, currentColor, targetColor, time = 2, delay = 0)
    {
        if (!this.tweenList)
        {
            this.tweenList = [];
        }
        const currentColorData = this.toRGB(currentColor);
        const targetColorData = this.toRGB(targetColor);
        const black = this.toRGB(0x000000);

        this.tweenList.push({
            tween: TweenLite.to(currentColorData, time, {
                delay,
                r: targetColorData.r,
                g: targetColorData.g,
                b: targetColorData.b,
                onUpdate: function (targ)
                {
                    currentColorData.r = Math.floor(currentColorData.r);
                    currentColorData.g = Math.floor(currentColorData.g);
                    currentColorData.b = Math.floor(currentColorData.b);
                    targ.tint = this.rgbToColor(currentColorData.r, currentColorData.g, currentColorData.b);
                }.bind(this),
                onUpdateParams: [target],
            }),
            target,
        });
    }

    toRGB(rgb)
    {
        const r = rgb >> 16 & 0xFF;
        const g = rgb >> 8 & 0xFF;
        const b = rgb & 0xFF;

        return {
            r,
            g,
            b,
        };
    }

    rgbToColor(r, g, b)
    {
        return r << 16 | g << 8 | b;
    }

    build(data)
    {
        this.data = data;
    }

    show(delay)
    {

    }

    hide(delay)
    {

    }

    resize(w, h)
    {
        this.x = w / 2;
        this.y = h / 2;

        this.topGradient.width = w;
        // this.topGradient.width = h / this.topGradient.height;
        this.bottomGradient.width = w;
        // this.bottomGradient.scale.y = -this.topGradient.scale.y;
        // this.width = w;
        // this.height = h;
        // console.log(this.smokeList);

        // for (const key in this.smokeList) {
        //     this.smokeList[key].x = w / 2
        //     this.smokeList[key].y = h / 2
        // }
    }
}
