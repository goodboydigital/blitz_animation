import tween from 'gsap/src/minified/TweenMax.min.js';
import Ring from './Ring';
export default class PIXIRing extends PIXI.Container{
  constructor() {
    super()
    this._ring = null;
    this.path = `assets/image/`;
  }
  setAssetsPath(path) {
    this.path = path;
  }
  load(cb) {
    cb();
    // const loader = new PIXI.loaders.Loader();
    // loader.add(`displacement_map`,`${this.path}/displacement_map.png`)
    // loader.load(()=>{
    //   if(cb) {
    //     cb();
    //   }
    // });
  }
  createRing(params) {
    this._ring = new Ring(params);
    return this._ring;
  }
  get ring() {
    return this._ring;
  }
  setPrice(number) {
    if(!this._ring) return;

    this._ring.setPrice(number)

  }
  pulse() {
    if(!this._ring) return;

    TweenLite.killTweensOf(this.ring.text.scale)
    TweenLite.to(this.ring.text.scale,0.25, {
      x:this.ring.text.originalScale + 0.5,
      y:this.ring.text.originalScale + 0.5,
      ease:Quad.easeOut
    })
    TweenLite.to(this.ring.text.scale,0.25, {
      x:this.ring.text.originalScale,
      y:this.ring.text.originalScale,
      delay:0.35,
      ease:Quad.easeOut
    })

    TweenLite.killTweensOf(this.ring.backSprite.scale)
    TweenLite.to(this.ring.backSprite.scale,0.25, {
      x:1.1,
      y:1.1,
      ease:Quad.easeOut
    })
    TweenLite.to(this.ring.backSprite.scale,0.25, {
      x:1,
      y:1,
      delay:0.35,
      ease:Quad.easeOut
    })
    this.ring.particles.spawn();
  }
}

// export default new PIXIRing();
