export default class Star extends PIXI.Container
{
    constructor(manager)
    {
        super();
        this.manager = manager;
    }

    build()
    {
        const sprite = new PIXI.Sprite.from('assets/image/star.png');

        const spriteFx = new PIXI.Sprite.from('assets/image/p3.png');

        sprite.addChild(spriteFx);
        spriteFx.anchor.set(0.5);
        spriteFx.scale.set(0.5);
        sprite.fx = spriteFx;
        if (Math.random() < 0.8) sprite.fx.visible = false;

        // sprite.blendMode = PIXI.BLEND_MODES.ADD;
        sprite.alpha = 0.75;
        this.addChild(sprite);
        sprite.anchor.x = 0.5;
        sprite.anchor.y = 1;
        sprite.scale.set(0.02);
        sprite.tint = this.manager.color;
        this.sprite = sprite;
    }

    reset(color, randomise = false)
    {
        (randomise) ? this.color = Math.random() * 16777215 : this.color = color;
        this.force = Math.random() * this.force;
        this.sprite.tint = this.color;
        this.sprite.scale.y = this.minScaleY;
    }

    update()
    {
        this.force = this.manager.globalForce + this.minScaleY * 2;
        if (this.forceIdle)
        {
            this.force = 0.1;
            this.isIdle = true;
        }
        // this.force = Math.min(this.force += 0.1, this.force)
        this.x += Math.cos(this.angle) * this.force;
        this.y += Math.sin(this.angle) * this.force;

        this.alpha = Math.lerp(1, this.alpha, 0.5);

        const dist = Math.hypot(this.starterPos.x - this.x, this.starterPos.y - this.y);

        this.sprite.rotation = this.angle + Math.PI / 2;

        this.sprite.scale.y = this.minScaleY * this.force * 6;
        // this.sprite.scale.x =

        this.sprite.scale.y = Math.max(this.sprite.scale.y, this.minScaleY);

        this.force = Math.max(this.force, 0.02);
        this.sprite.scale.x = this.minScaleY + this.minScaleY * this.force;

        if (this.isIdle)
        {
            this.sprite.alpha = this.minScaleY * 10;
        }
        else
        {
            this.sprite.alpha = 0.75;
            this.sprite.fx.visible = false;
            // this.sprite.scale.x = this.minScaleY * this.manager.starWidth;
            this.sprite.scale.x = this.minScaleY + this.minScaleY * this.force * this.manager.starWidth;
        }

        // if(this.counter < 100)
        // this.force += 0.01
        // else this.force -= 0.01
    }
}
