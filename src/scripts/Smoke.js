import * as PIXI from 'pixi.js';
import TweenLite from 'gsap'
export default class Smoke extends PIXI.Container {
    constructor(background) {
        super();
        this.background = background;
    }
    build(data) {
        this.data = data;
        this.smokeFx = PIXI.Sprite.from('assets/image/smoke_'+ this.data.type + "_" + this.data.num + '.jpg')
        this.addChild(this.smokeFx)
        this.smokeFx.scale.x = this.data.width / this.smokeFx.width
        this.smokeFx.scale.y = this.data.height / this.smokeFx.height
        this.smokeFx.blendMode = PIXI.BLEND_MODES.ADD
        this.smokeFx.alpha = 0
        this.smokeFx.anchor.set(0.5)
        this.smokeFx.scale.set(2.5)
        this.originPos = {
            x: this.x,
            y: this.y
        }
        this.reset()
        this.alphaLimit = 0.5
        // this.scaleLimit = 5
    }
    reset(){
        this.speed = Math.random() * 0.005 + 0.05;
        this.speedScale = Math.random() * 0.005 + 0.005
        this.disipate = true;
        this.disipateSpeed = Math.random() * 0.005

        this.speedRot = Math.random() * 0.04 - 0.02
        this.speedRot *= 0.05
        this.speedScale *= 0.15

        this.sin = Math.random() * 5
        this.sinScale = Math.random() * 5
        this.alphaLimit = 0.25;
        this.scaleLimit = 7
    }
    warpping(){
        
        this.speedRot *= 10
        this.speed*= 2
        this.alphaLimit = 0.75
        this.scaleLimit = 10
    }
    update() {

        this.sin += this.speed
        this.sinScale += this.speedScale
        const n = (Math.sin(this.sin) * 0.5 + 0.5)
        const n2 = (Math.sin(this.sinScale) * 0.5 + 0.5)

        this.sin %= Math.PI * 2
        // this.y = n2;
        this.x = n;
        // console.log(n2);
        
        this.smokeFx.rotation += this.speedRot
        // if (this.disipate){
        //     this.smokeFx.alpha -= 1/60 * this.disipateSpeed
        // }else{
        //     this.smokeFx.alpha += 1/60 * this.disipateSpeed
        // }
        
        // if (this.smokeFx.alpha <= 0){
        //     this.disipate = false;
        //     this.smokeFx.texture = PIXI.Texture.from('assets/image/smoke_'+ this.data.type + "_" + Math.floor(Math.random() * 2 + 1) + '.jpg')
        // }else if (this.smokeFx.alpha >= 0.025){
        //     this.disipate = true;
        // }
        this.smokeFx.alpha = (n) * this.alphaLimit + this.alphaLimit * 0.5
        this.smokeFx.scale.set((n2) * (this.scaleLimit*0.5) + (this.scaleLimit*0.5))
    }
    show(delay) {
        if (this.data.type == 'top') {
            this.smokeFx.tint = this.background.colorSet.topSmokeStandard;

        } else {
            this.smokeFx.tint = this.background.colorSet.bottomSmokeStandard;
        }
        // TweenLite.to(this, 0.25, {
        //     x: this.originPos.x,
        //     y: this.originPos.y,
        //     alpha: 1,
        //     ease: Quad.easeOut
        // })
    }
    hide(delay) {
        if (this.data.type == 'top') {
            this.smokeFx.tint = this.background.colorSet.topSmokeWarp;
        } else {
            this.smokeFx.tint = this.background.colorSet.bottomSmokeWarp;
        }
        // TweenLite.to(this, 0.25, {
        //     x: 0,
        //     y: 0,
        //     alpha: 0,
        //     ease: Quad.easeOut
        // })
    }
}