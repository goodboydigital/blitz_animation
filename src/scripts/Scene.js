import * as PIXI from 'pixi.js';

import dat from 'dat.gui/build/dat.gui.min.js';
import PIXIRing from './PIXIRing';
import Ring from './Ring';
import StarWarp from './StarWarp';
import Background from './Background';
import FilterManager from './FilterManager';

import BasicGame from 'odie/BasicGame';
import Camera from 'odie/components/view3d/Camera';
import OrbitalCamera from 'odie/components/view3d/OrbitalCamera';
import PostSystem from 'odie/components/view3d/PostSystem';
import CoinManager from './CoinManager';
import Light from 'odie/components/view3d/Light';
// import data from './'

export default class Scene
{
    constructor(w, h)
    {
        this.renderer = new PIXI.Renderer(w, h, {
            backgroundColor: 0x000000,
        });
        window.renderer = this.renderer;
        this.renderer.view.className = 'experiment';
        document.body.appendChild(this.renderer.view);

        this.stage = new PIXI.Container();
        const background = new PIXI.Sprite.from(`assets/image/star-background.jpg`);

        background.scale.set(1.4);
        // this.stage.addChild(background);

        this.ring = new Ring();
        this.ring.position.x = this.renderer.view.width / 2;
        this.ring.position.y = this.renderer.view.height / 2;

        this.background = new Background(this, this.renderer.view.width, this.renderer.view.height);
        this.starWarp = new StarWarp(this, this.renderer.view.width, this.renderer.view.height);

        this.container3D = new PIXI.Container();
        this.stage.addChild(this.background);

        this.stage.addChild(this.starWarp);
        this.stage.addChild(this.ring);
        this.stage.addChild(this.container3D);

        // const camera = new OrbitalCamera();
        // const camera = new Camera({ far: 800, fov: 50 })//OrbitalCamera();
        this.camera = new Camera(); // OrbitalCamera();

        // this.fov = camera.camera.fov;

        this.game = new BasicGame({
            stage: this.container3D,
            camera: this.camera,
            renderer: this.renderer,
        });
        // this.game.addSystem(PostSystem, { stage: this.container3D });
        // console.log(this.game.post);
        this.container3D.alpha = 0.15;
        // const game = new BasicGame({ stage:this.container3D, camera, renderer:this.renderer });
        this.spot = new Light({
            color: 0xFFFFFF,
            intensity: 1.4,
            type: 1,
            distance: 150,
            debug: false,
        });
        this.game.addChild(this.spot);

        // spot.y = -5
        // spot.z = -50
        // this.game.view3d.globalUniforms.uniforms.uFogColor = new Float32Array([1, 1, 0.2]);
        this.game.view3d.ambientLight.intensity = 0.5;// 0.75;
        this.game.view3d.ambientLight.color = 0xFFFFFF;

        this.filterManager = new FilterManager(this, { width: this.w, height: this.h });
        this.coinManager = new CoinManager(this.game);
        this.coinManager.build();

        this.buildGUI();
        this.render();
        this.starWarp.isIdleSignal.add((idle) =>
        {
            this.background.setWarpEffect(idle);
            this.filterManager.pulse();
            this.coinManager.isWarping = !idle;
            // this.setGlobalColour();
            this.ring.setWarping(idle);
            this.filterManager.setBlurEffect(idle);
        });

        console.log(this.game.view3d);
    }

    setGlobalColour()
    {
        const firstColour = this.background.toRGB(this.background.topGradient.tint);
        const secondColour = this.background.toRGB(this.background.bottomGradient.tint);

        const gradient = this.getGlobalColour(0.5, firstColour, secondColour);

        const lightColour = this.background.rgbToColor(gradient[0], gradient[1], gradient[2]);

        this.spot.light.color = lightColour;
    // this.renderer.backgroundColor = lightColour
    }

    getGlobalColour(p, rgb_beginning, rgb_end)
    {
        const w = p * 2 - 1;
        const w1 = (w + 1) / 2.0;
        const w2 = 1 - w1;

        const rgb = [Math.floor(rgb_beginning.r * w1 + rgb_end.r * w2),
            Math.floor(rgb_beginning.g * w1 + rgb_end.g * w2),
            Math.floor(rgb_beginning.b * w1 + rgb_end.b * w2),
        ];

        return rgb;
    }

    applyJson()
    {
    // console.log(  );

        const json = window.resourcesData['assets/json/defaultData.json'].data.remembered.Default;

        // debugger
        let count = 0;

        for (const key in json)
        {
            // if (json.hasOwnProperty(key)) {
            const element = json[key];

            for (const key2 in element)
            {
                const element2 = element[key2];

                this.rememberList[count][key2] = element2;
            }
            count++;

            // }
        }
    }

    buildGUI()
    {
    // ///////////////////////////////////////////////RING
        this.scale = 1;
        const debug = true;

        this.rememberList = [
            this,
            this.starWarp,
            this.background.colorSet,
            this.coinManager,
            this.coinManager.coinSpeed,
            this.coinManager.rotationSpeed,
            this.coinManager.distances,
            this.camera.camera,
            this.filterManager,
            this.filterManager.cornerBlur,
        ];
        this.applyJson();

        if (debug)
        {
            this.gui = new dat.GUI();

            for (let index = 0; index < this.rememberList.length; index++)
            {
                const element = this.rememberList[index];

                console.log(element);

                this.gui.remember(element);
            }

            // ////////////////////////////////////////////////////CAMERA
            this.cameraGUI = this.gui.addFolder('Camera');
            this.cameraGUI.add(this.camera.camera, 'fov').min(30).max(120);

            // /////////////////////////////////////////////////////WARP
            this.warpGUI = this.gui.addFolder('Warp');

            this.warpGUI.add(this.starWarp, 'starAmount').min(0).max(this.starWarp.starAmount);
            this.warpGUI.addColor(this.starWarp, 'color');
            this.warpGUI.add(this.starWarp, 'randomiseColor');

            this.warpGUI.add(this.starWarp, 'stop');

            this.warpSpeed = this.starWarp.defaultForce;
            this.warpGUI.add(this, 'warpSpeed').min(1).max(100).onChange(() =>
            {
                if (!this.starWarp.isIdle) this.starWarp.globalForce = this.warpSpeed;
                if (!this.starWarp.isIdle) this.starWarp.force = this.warpSpeed;
            });

            this.warpGUI.add(this.starWarp, 'starWidth').min(0.0005).max(0.3);

            this.warpGUI.add(this.starWarp, 'stopWarpToggle');
            this.warpGUI.add(this.starWarp, 'startWarpToggle');
            this.warpGUI.add(this, 'resetWarp');

            // ///////////////////////////////////////////////BACKGROUND
            this.backgroundGUI = this.gui.addFolder('Background');
            this.backgroundGUI.add(this.background, 'annulusSpeed').min(0).max(10);

            // this.gui.remember(this.background.colorSet)
            for (const key in this.background.colorSet)
            {
                this.backgroundGUI.addColor(this.background.colorSet, key);
            }

            // ////////////////////////////////////////////////COINS
            // this.gui.remember(this.coinManager)
            // this.gui.remember(this.coinManager.coinSpeed)
            this.coinGUI = this.gui.addFolder('Coin');
            this.coinGUI.add(this.coinManager, 'totCoins').min(0).max(100);
            this.coinGUI.add(this.coinManager, 'coinScale').min(0.5).max(3);
            this.coinGUI.add(this.coinManager, 'swapCurrency');
            this.coinSpeedGUI = this.coinGUI.addFolder('Movement Speed');
            this.coinSpeedGUI.add(this.coinManager.coinSpeed, 'standard').min(0.1).max(15);
            this.coinSpeedGUI.add(this.coinManager.coinSpeed, 'warp').min(0.1).max(15);
            this.coinRotationGUI = this.coinGUI.addFolder('Rotation Speed');

            this.coinRotationGUI.add(this.coinManager.rotationSpeed, 'x').min(0).max(15);
            this.coinRotationGUI.add(this.coinManager.rotationSpeed, 'y').min(0).max(15);
            this.coinRotationGUI.add(this.coinManager.rotationSpeed, 'z').min(0).max(15);

            this.coinSpawnGUI = this.coinGUI.addFolder('Spawn Radius');
            this.coinSpawnGUI.add(this.coinManager.distances, 'min').min(1).max(30);
            this.coinSpawnGUI.add(this.coinManager.distances, 'max').min(1).max(30);

            // ///////////////////////////////////////////////////FILTER
            this.filterGUI = this.gui.addFolder('Filters');
            this.filterGUI.add(this.filterManager, 'blurStrengthWarp').min(0).max(1);
            this.filterGUI.add(this.filterManager, 'blurStrengthIdle').min(0).max(1);
            this.filterGUI.add(this.filterManager.cornerBlur, 'innerRadius').min(0).max(this.filterManager.cornerBlur.radius);
            this.filterGUI.add(this.filterManager.cornerBlur, 'radius').min(0).max(this.filterManager.cornerBlur.radius);
        }
    // this.ringGUI.add(this, 'pulse')

    // ///////////////////////////////////////////////WARP
    }

    render()
    {
        window.requestAnimationFrame(this.render.bind(this));
        this.renderer.render(this.stage);
        this.background.update();
        this.starWarp.update();
        this.filterManager.update();
        this.game.update();

        this.coinManager.update();
    // this.coin.update(1/60)
    }

    pulse()
    {
        PIXIRing.pulse();
    }

    resetWarp()
    {
        this.starWarp.globalForce = 0;
    }

    resize(w, h)
    {
        let scale = 1; // window.devicePixelRatio;

        if (scale > 2)
        {
            scale = 2;
        }
        this.renderer.resize(w * scale, h * scale);
        this.w = this.renderer.view.width;
        this.h = this.renderer.view.height;

        if (this.ring)
        {
            this.ring.x = w / 2;
            this.ring.y = h / 2;

            // this.container3D.x= -w / 2;
            // this.container3D.y= -h / 2;
        }

        this.filterManager.resize({
            width: w,
            height: h,
        });

        if (this.starWarp)
        {
            this.starWarp.resize(w, h);
        }

        if (this.background)
        {
            this.background.resize(w, h);
        }
    }
}
