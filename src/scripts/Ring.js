import * as PIXI from 'pixi.js';
import Light from './Light';
import Particles from './Particles';

function random(min, max)
{
    return Math.random() * (max - min) + min;
}

export default class Ring extends PIXI.Container
{
    constructor({
        currency = '€',
        number = 10,
        frameSpeed = 5,
        rotation = 0.010,
        numberLightSide = 20,
        numberLightCircle = 180,
        colors = [
            0x686bec,
            0x4451f6,
            0x5e69f7],
        displacement = 60,
        blur = 0,
        textStyle = {
            fill: 'white',
            fontSize: 74,
            fill: ['#bcc1ff', '#4955ff'],
            fillGradientStops: [0.3],
            fillGradientType: 0,
            stroke: 'white',
            strokeThickness: 5,
        },
    } = {})
    {
        super();
        this.currency = currency;
        this.rotation = rotation;
        this.spawnFrame = 40;
        this.frameSpeed = frameSpeed;
        this.frame = 0;
        this.time = 0;

        this.divisor = 1;
        this.w = 512;
        this.h = 512;
        this.back = new PIXI.Container();
        this.front = new PIXI.Container();

        const backgroundBack = new PIXI.Graphics();

        backgroundBack.beginFill(0x000000);
        backgroundBack.drawRect(0, 0, this.w, this.h);
        this.back.addChild(backgroundBack);

        this.aura = new PIXI.Sprite.from('aura.png');
        this.aura.tint = 0x686bec;
        this.aura.scale.set(2.6);
        this.aura.anchor.set(0.5);
        this.aura.position.x = this.w / 2;
        this.aura.position.y = this.h / 2;
        this.back.addChild(this.aura);

        this.bg = new PIXI.Sprite.from('background.png');
        this.bg.tint = 0x686bec;
        this.bg.scale.set(2.6);
        this.bg.anchor.set(0.5);
        this.bg.position.x = this.w / 2;
        this.bg.position.y = this.h / 2;
        this.back.addChild(this.bg);

        this.renderTexture = new PIXI.RenderTexture.create(this.w, this.h, PIXI.SCALE_MODES.LINEAR, 1);
        this.backSprite = new PIXI.Sprite.from(this.renderTexture);
        this.backSprite.anchor.set(0.5);
        this.backSprite.blendMode = PIXI.BLEND_MODES.SCREEN;
        this.addChild(this.backSprite);

        this.particles = new Particles();
        this.addChild(this.particles);

        this.addChild(this.front);

        this.addText(textStyle);
        this.addContainers();

        this.addSprites(numberLightSide, numberLightCircle, colors);
        this.addFilters(displacement, blur);

        this.setPrice(number);

        this.fakeBloom = new PIXI.Sprite.from('bloom.png');
        this.fakeBloom.blendMode = PIXI.BLEND_MODES.ADD;
        this.fakeBloom.scale.set(2);
        this.fakeBloom.anchor.set(0.5);
        this.fakeBloom.position.x = this.w / 2;
        this.fakeBloom.position.y = this.h / 2;
        this.back.addChild(this.fakeBloom);

    // console.log(this);
    }

    setWarping(idle)
    {
        if (idle)
        {
            this.fakeBloom.tint = 0x686bec;
        }
        else
        {
            this.fakeBloom.tint = 0x000000;
        }
    }

    addFilters(displacement, blur)
    {
        const map = new PIXI.Sprite.from(`assets/image/displacement_map.png`);

        map.scale.set(2);
        this.back.addChild(map);
        map.anchor.set(0.5);
        map.x = this.w / 2;
        map.y = this.h / 2;
        this.map = map;
        this.displacement = new PIXI.filters.DisplacementFilter(map);
        this.displacement.scale.x = displacement;
        this.displacement.scale.y = displacement;

        // const bloom = this.bloom = new BloomFilter();
        // bloom.blurX = blur;
        // bloom.blurY = blur;

        // this.back.filters = [this.bloom, this.displacement]
        this.back.filters = [this.displacement];
    }

    addContainers()
    {
        this.containers = [];
        for (let i = 0; i < 8; i++)
        {
            const container = new PIXI.Container();
            const containerCenter = new PIXI.Container();

            containerCenter.position.x = -this.w / 2;
            containerCenter.position.y = -this.h / 2;
            container.position.x = this.w / 2;
            container.position.y = this.h / 2;

            container.addChild(containerCenter);

            this.containers.push({
                container,
                center: containerCenter,
                r: random(-1, 1),
            });
            this.back.addChild(container);
        }
    }

    addSprites(numberLightSide, numberLightCircle, colors)
    {
        this.sprites = [];
        for (var i = 0; i < numberLightSide; i++)
        {
            const sprite = new Light({ x: 0, y: 0.5 }, 40, colors, 0.2, [160, 170], [45, 90], true);

            sprite.resize(this.w, this.h);
            sprite.init();
            this.back.addChild(sprite.view);
            this.sprites.push(sprite);
        }

        this.sprites2 = [];
        for (var i = 0; i < numberLightCircle; i++)
        {
            const sprite = new Light({ x: 0.5, y: 0.5 }, 40, colors, 0.4, [160, 200], [-90, 90], false);

            sprite.resize(this.w, this.h);
            sprite.init();
            this.sprites2.push(sprite);
            const container = this.containers[Math.floor(Math.random() * this.containers.length)].center;

            container.addChild(sprite.view);
        }
    }

    addText(style)
    {
        const text = new PIXI.Text('€10.00', style);

        text.anchor.set(0.5);
        text.originalScale = 1;
        this.text = text;
        this.front.addChild(text);
    }

    updateTransform()
    {
        super.updateTransform();
        // console.log('RING');

        this.time += 0.1;
        this.frame++;

        for (var i = 0; i < this.sprites.length; i++)
        {
            const sprite = this.sprites[i];

            sprite.count++;

            if (this.frame % Math.floor(this.frameSpeed) === 0)
            {
                sprite.view.texture = new PIXI.Texture.from(`lightning_0${Math.floor(Math.random() * 7 + 1)}.png`);
            }

            if (sprite.count > this.spawnFrame)
            {
                sprite.reset();
            }
        }

        for (var i = 0; i < this.sprites2.length; i++)
        {
            const sprite = this.sprites2[i];

            sprite.count++;
            if (this.frame % Math.floor(this.frameSpeed) === 0)
            { sprite.view.texture = new PIXI.Texture.from(`lightning_0${Math.floor(Math.random() * 7 + 1)}.png`); }

            if (sprite.count > this.spawnFrame)
            {
                sprite.reset();
            }
        }

        for (var i = 0; i < this.containers.length; i++)
        {
            this.containers[i].container.rotation += this.rotation * this.containers[i].r;
        }
        this.map.rotation += 0.02;

        renderer.render(this.back, this.renderTexture);

        this.aura.scale.x = 2.6 + Math.cos(this.time) * 0.1;
        this.aura.scale.y = 2.6 + Math.sin(this.time / 2) * 0.1;

        this.bg.scale.x = 2.3 + Math.cos(this.time / 1.5) * 0.2;
        this.bg.scale.y = 2.3 + Math.sin(this.time / 2.0) * 0.2;

        this.fakeBloom.scale.x = 1.8 + Math.cos(this.time / 1.5) * 0.2;
        this.fakeBloom.scale.y = 1.8 + Math.sin(this.time / 1.5) * 0.2;
        this.fakeBloom.rotation += 0.001;
    }

    setPrice(number)
    {
        if (number > 1000)
        {
            this.text.originalScale = 0.8;
        }
        if (number > 10000)
        {
            this.text.originalScale = 0.7;
        }
        if (number > 100000)
        {
            this.text.originalScale = 0.65;
        }
        this.text.text = `${this.currency}${number.toFixed(2)}`;
    }
}
