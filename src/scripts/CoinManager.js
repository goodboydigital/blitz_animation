import * as PIXI from 'pixi.js';
import Coin3D from './Coin3D';

export default class CoinManager
{
    constructor(game)
    {
        this.coins = [];

        this.rotationSpeed = {
            x: 2,
            y: 2,
            z: 2,
        };

        this.coinSpeed = {
            standard: 0.75,
            warp: 2,
        };

        this.game = game;

        this.totCoins = 14;
        this.coinScale = 3.5;
        this.isWarping = true;

        this.distances = {
            min: 15,
            max: 20,
        };
    }

    swapCurrency()
    {
        for (const key in this.coins)
        {
            const element = this.coins[key];

            element.textureIndex++;
            element.textureIndex %= element.textures.length;

            element.body.view3d.material.map = PIXI.Texture.from(element.textures[element.textureIndex]);
        }
    }

    addCoin()
    {
        const coin = new Coin3D();

        this.game.addChild(coin);
        this.coins.push(coin);
        coin.reset();
        coin.z = -100 * Math.random();
    }

    removeCoin()
    {
        this.game.removeChild(this.coins[this.coins.length - 1]);
        this.coins.pop();
    }

    build()
    {
        // for (let index = 0; index < 14; index++)
        // {
        //     this.addCoin();
        //     // coin.x = Math.random() * 8 - 4
        //     // coin.y = Math.random() * 4 - 2
        // }
    }

    update()
    {
        if (this.totCoins > this.coins.length)
        {
            this.addCoin();
        }
        else if (this.totCoins < this.coins.length)
        {
            this.removeCoin();
        }
        for (let index = 0; index < this.coins.length; index++)
        {
            const element = this.coins[index];

            this.setData(element);
            element.update(1 / 60);
        }
    }

    setData(element)
    {
        if (this.isWarping)
        {
            element.speedZ = this.coinSpeed.warp;
            element.opacityScaleMult = 1.5;
        }
        else
        {
            element.speedZ = this.coinSpeed.standard;
            element.opacityScaleMult = 1;
        }

        element.rotationSpeed.x = this.rotationSpeed.x;
        element.rotationSpeed.y = this.rotationSpeed.y;
        element.rotationSpeed.z = this.rotationSpeed.z;
        element.coinScale = this.coinScale;
        element.minDist = this.distances.min;
        element.maxDist = this.distances.max;
    }
}
