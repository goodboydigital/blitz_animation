export default class Particles extends PIXI.Container {
  constructor() {
    super();
    this.pool = [];
    this.particles = [];
    this.container = new PIXI.Container();
    this.addChild(this.container)
  }
  spawn(number = 60) {
    for (var i = 0; i < number; i++) {
      this.addParticles();
    }
  }
  addParticles() {


    var particles_tex = [`star.png`];

    var particles = this.pool.pop();

    if(!particles){

      particles = new PIXI.Sprite.from(particles_tex[Math.floor(Math.random()* particles_tex.length)]);
      // console.log(particless_tex.length % this.indexBubble);
      particles.anchor.set(.5);
      particles.blendMode = PIXI.BLEND_MODES.SCREEN;
      particles.speed = new PIXI.Point();
    }

    particles.scale.x = particles.scale.y = 0;
    var scale = Math.random() * (1.8 - 0.5) + 0.5;
    TweenLite.to(particles.scale, 1, {
      x: scale,
      y: scale,
      ease: Elastic.easeOut
    });

    particles.alpha = Math.random() * .3 + .69;
    particles.position.x =  0;
    particles.position.y = 0;
    particles.speed.x =  Math.random() * (20 + 20) - 20;
    particles.speed.y =  Math.random() * (20 + 20) - 20;
    particles.amplitude = Math.random();
    particles.tick = Math.random() * Math.PI * 2;
    particles.tickLife = Math.random() *0.05 + 0.01;

    this.container.addChild(particles);
    this.particles.push(particles);

  }
  updateTransform() {
    super.updateTransform();
    this.count += 0.02;
  this.tick++;
  let spawn = 10;

  if(this.tick % spawn === 0) {
    // this.addBubble();
  }


  for (var i = 0; i < this.particles.length; i++) {
    var b = this.particles[i];
    b.tick++;
    b.position.x += b.speed.x;
    b.position.y += b.speed.y;
    b.speed.x *= 0.94;
    b.speed.y *= 0.94;
    b.rotation += 0.1;
    b.speed.y += 0.3;
    b.alpha -= b.tickLife;

    if(b.alpha < 0.05){
      this.pool.push(b);
      this.particles.splice(i, 1);
      this.container.removeChild(b);

      i--;
    }
  }
  }
}
