import Star from './Star';
import Signals from 'signals';
export default class StarWarp extends PIXI.Container
{
    constructor(scene, w, h)
    {
        super();
        this.scene = scene;
        this.screenWidth = w;
        this.screenHeight = h;
        this.starAmount = 500;
        this.defaultForce = 55;
        this.force = this.defaultForce;
        this.stars = [];
        this.starsIdle = [];
        this.alphaValue = 1;
        this.frequency = this.starAmount;
        this.color = 0xFFFFFF;
        this.stop = false;
        this.hideWarp = false;
        this.lineWidth = 3;
        this.starWidth = 0.15;
        this.isIdle = false;

        this.center = {
            x: 0, // this.screenWidth / 2,
            y: 0, // this.screenHeight / 2,
        };
        console.log(this.screenHeight, this.screenWidth, 'Dimensions');

        this.build();

        this.x = this.center.x;
        this.y = this.center.y;

        this.isIdleSignal = new Signals();

        this.randomiseColor = false;
        this.skipToggle = false;
    }

    build()
    {
        for (let i = 0; i < this.starAmount; i++)
        {
            const star = new Star(this);

            star.build();
            this.stars.push(star);
            this.addChild(star);
            this.randomizeStar(star);
            star.forceIdle = false;
        }

        for (let i = 0; i < this.starAmount / 2; i++)
        {
            const star = new Star(this);

            star.build();
            this.starsIdle.push(star);
            this.addChild(star);
            this.randomizeStar(star);
            star.forceIdle = true;
        }
        console.log('BUILD');

        this.counter = 0;
        this.globalForce = 0;

        setTimeout(() =>
        {
            this.starIdle(true);
        }, 100);

        this.defaultLerpSpeed = 0.995;
        this.forceLerpSpeed = this.defaultLerpSpeed;
    }

    randomizeStar(star)
    {
        this.center.x = 0;
        this.center.y = 0;
        // star.x = Math.random() * this.screenWidth - this.screenWidth / 2; // Math.cos(angle) * distance;
        // star.y = Math.random() * this.screenHeight - this.screenHeight / 2; // Math.sin(angle) * distance;

        // star.z =
        let angle = Math.random() * Math.PI * 2;

        const dist = Math.random() * (this.screenWidth / 2);

        star.x = Math.cos(angle) * dist;
        star.y = Math.sin(angle) * dist;

        angle = Math.atan2(this.center.y - star.y, this.center.x - star.x) + Math.PI; // Math.random() * Math.PI * 2;

        star.force = 1;
        star.angle = angle;
        star.sprite.rotation = star.angle + Math.PI / 2;
        star.sprite.fx.rotation = -star.sprite.rotation;
        star.minScaleY = (Math.random() * 0.03) + 0.01;
        star.sprite.fx.scale.set((star.minScaleY / star.sprite.scale.x) * 5);
        star.sprite.scale.x = star.minScaleY;
        star.starterPos = {
            x: star.x,
            y: star.y,
        };
    }

    update()
    {
        if (this.starAmount > this.stars.length)
        {
            this.addStar();
        }
        else if (this.starAmount < this.stars.length)
        {
            this.removeStar();
        }
        // return
        if (this.stop) return;
        this.counter++;

        // this.globlForce += 0.5

        // this.globlForce = Math.min(this.force, this.globlForce)

        this.globalForce = Math.lerp(this.force, this.globalForce, this.forceLerpSpeed);

        this.updateList(this.stars);
        this.updateList(this.starsIdle);
    }

    updateList(list)
    {
        for (let i = 0; i < list.length; i++)
        {
            const element = list[i];

            element.update();
            if ((element.x + element.width) <= -(this.screenWidth / 2) || (element.x - element.width) > (this.screenWidth / 2))
            {
                this.resetPosition(element);
            }

            if ((element.y + element.height) <= -(this.screenHeight / 2) || (element.y - element.height) > (this.screenHeight / 2))
            {
                this.resetPosition(element);
            }

            if (this.hide)
            {
                // element.alpha = 0;
            }
        }
    }

    starIdle(idle)
    {
        this.isIdleSignal.dispatch(idle);

        if (idle)
        {
            this.isIdle = true;
            this.forceLerpSpeed = 0.6;
            this.force = 0;

            for (const key in this.stars)
            {
                if (Math.random() < 0.2) this.stars[key].sprite.fx.visible = true;
            }
        }
        else
        {
            this.isIdle = false;
            this.forceLerpSpeed = this.defaultLerpSpeed;
            this.force = this.defaultForce;
        }

        setTimeout(() =>
        {
            if (!this.skipToggle)
            {
                this.starIdle(!this.isIdle);
            }
        }, 5000);
    }

    stopWarpToggle()
    {
        this.skipToggle = true;
    }

    startWarpToggle()
    {
        this.skipToggle = false;
        this.starIdle(!this.isIdle);
    }

    resetPosition(star)
    {
        star.alpha = 0;
        this.randomizeStar(star);
        star.reset(this.color, this.randomiseColor);
    }

    warpState(state)
    {
        this.stop = state;
    }

    resize(w, h)
    {
        this.screenWidth = w;
        this.screenHeight = h;
        this.x = w / 2;
        this.y = h / 2;
        this.center = {
            x: this.screenWidth / 2,
            y: this.screenWidth / 2,
        };

        for (const key in this.starsIdle)
        {
            const element  = this.starsIdle[key];

            this.randomizeStar(element);
        }
    }

    addStar()
    {
        const star = new Star(this);

        star.build();
        this.stars.push(star);
        this.addChild(star);
        this.randomizeStar(star);
    }

    removeStar()
    {
        console.log('remove');
        this.removeChild(this.stars[this.stars.length - 1]);
        this.stars.pop();
    }
}
