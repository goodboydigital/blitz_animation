function random(min, max) {
    return Math.random() * (max - min) + min;
}
import * as PIXI from 'pixi.js';
export default class Light {
  constructor(anchor,count, colors,scaleView, rangePos, rangeAngle, offsetA) {
    const sprite = new PIXI.Sprite.from('lightning_01.png')
    sprite.anchor.set(anchor.x,anchor.y);
    sprite.blendMode = PIXI.BLEND_MODES.ADD;
    this.count = Math.random() * count;
    this.view = sprite;

    this.colors = colors;
    this.rangePos = rangePos;
    this.rangeAngle = rangeAngle;


    this.offsetA = offsetA;
    this.scaleView = scaleView;
  }
  init() {
    this.view.tint = this.colors[Math.floor(Math.random()*this.colors.length)];
    const angle = Math.random() * Math.PI * 2;
    this.view.position.x = this.w/2 + Math.cos(angle) * random(this.rangePos[0],this.rangePos[1]);
    this.view.position.y = this.h/2 + Math.sin(angle) * random(this.rangePos[0],this.rangePos[1]);
    this.view.scale.set(Math.random() * this.scaleView)
    let dir = {
      x:this.w/2 - this.view.position.x,
      y:this.view.position.y - this.h/2,
    }
    if(this.offsetA) {
      this.view.rotation = Math.atan2(dir.x,dir.y) + Math.PI/180 * 90 + ( Math.PI/180 * random(-this.rangeAngle[0],this.rangeAngle[1]))

    } else {
      this.view.rotation = Math.atan2(dir.x,dir.y);
    }

  }
  reset(radius = 0) {
    this.count = 0;
    this.view.tint = this.colors[Math.floor(Math.random()*this.colors.length)];
    const angle = Math.random() * Math.PI * 2;
    this.view.position.x = this.w/2 + Math.cos(angle) * (random(this.rangePos[0],this.rangePos[1]) + radius);
    this.view.position.y = this.h/2 + Math.sin(angle) * (random(this.rangePos[0],this.rangePos[1]) + radius);
    this.view.scale.set(Math.random() * this.scaleView)
    let dir = {
      x:this.w/2 - this.view.position.x,
      y:this.view.position.y - this.h/2,
    }
    if(this.offsetA) {
      this.view.rotation = Math.atan2(dir.x,dir.y) + Math.PI/180 * 90 + ( Math.PI/180 * random(-this.rangeAngle[0],this.rangeAngle[1]))

    } else {
      this.view.rotation = Math.atan2(dir.x,dir.y);
    }



  }
  resize(w,h) {
    this.w = w;
    this.h = h;
  }
}
