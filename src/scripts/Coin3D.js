import * as PIXI from 'pixi.js';
import Entity3d from 'odie/Entity3d';
import PhongMaterial from 'odie/components/view3d/materials/PhongMaterial';
import SphereGeometry from 'odie/geometry/SphereGeometry';
import Light from 'odie/components/view3d/Light';
import { TweenLite } from 'gsap';
import lerp from 'lerp';

// let material = null;

export default class Coin3D extends Entity3d
{
    constructor()
    {
        super();

        this.textures = ['assets/image/euro_coin.png', 'assets/image/pound_coin.png', 'assets/image/dollar_coin.png', 'assets/image/yen_coin.png'];
        this.textureIndex = 0;
        this.color = 0xFFFFFF;

        // if (material === null)
        // {
        const material = new PhongMaterial({
            // color: this.color,
            opacity: 0,
            // emissive: 0,
            // shininess:0,
            specular: 0xFFFFFF,
            map: PIXI.Texture.from(this.textures[this.textureIndex]),
            // instancing: true,
        });

        console.log('material created');
        // }

        this.material = material;
        // debugger;
        const state = new PIXI.State();

        state.blend = true;
        // state.blendMode = PIXI.BLEND_MODES.ADD
        state.depthTest = true;
        state.culling = true;

        // console.log(PIXI.geometryCache);
        // debugger
        this.bodyContainer = new Entity3d();
        this.addChild(this.bodyContainer);

        this.body = new Entity3d(
            {
                geometry: PIXI.geometryCache['assets/models/coin-casino.gbo'],
                material: this.material,
                state,
            });
        this.bodyContainer.addChild(this.body);
        // debugger;
        // this.light = new Light({ color: this.color, intensity: 3, type: 1, distance: 35, debug: false });
        // this.light.z = -5
        // this.light.y = -3
        this.body.scale.set(0.02);
        this.body.rotation.x = Math.PI / 2;
        this.bodyContainer.scale.set(0.5);

        // this.body.addChild(this.light)
        this.sin = 0;
        this.n = 0;
        this.velZ = null;
        this.speedZ = null;
        this.coinScale = null;

        this.rotationSpeed = {
            x: 0,
            y: 0,
            z: 0,
        };
        this.minDist = null;
        this.maxDist = null;
    }

    reset(increaseAlpha = false)
    {
        this.material.uniforms.uOpacity = 0;

        if (increaseAlpha) TweenLite.to(this.material.uniforms, 1, { uOpacity: 1 });
        const randomDist = Math.random() * this.maxDist + this.minDist;

        this.sin = Math.random() * Math.PI * 2;
        this.z = -50 - Math.random() * 25;
        this.velZ = 0;
        this.angle = Math.random() * Math.PI * 2;
        this.x = Math.cos(this.angle) * randomDist;
        this.y = Math.sin(this.angle) * randomDist;
        TweenLite.killTweensOf(this.bodyContainer.scale);
        this.bodyContainer.scale.set(0);
        TweenLite.to(this.bodyContainer.scale, 1, { x: this.coinScale, y: this.coinScale, z: this.coinScale });
    }

    update(delta)
    {
        this.sin += delta * 15;

        this.velZ += delta * this.speedZ;
        // this.bodyContainer.y = Math.sin(this.sin) * 0.25;

        this.z += this.velZ;
        // this.bodyContainer.rotation.x = this.sin

        this.bodyContainer.rotation.y = this.velZ * this.rotationSpeed.y;
        this.bodyContainer.rotation.x = this.velZ * this.rotationSpeed.x;
        this.bodyContainer.rotation.z = this.velZ * this.rotationSpeed.z;
        // this.bodyContainer.rotation.z = -this.sin;
        this.n = Math.cos(this.sin) * 0.5 + 0.5;
        this.sin %= Math.PI * 2;

        // console.log(this.z);

        if (this.z > 8)
        {
            this.reset(true);
        }

        // console.log(this.z);
        // this.material.uniforms.uOpacity = lerp(this.material.uniforms.uOpacity, 1, ((this.speedZ * 0.02) * this.opacityScaleMult));

        // this.material.opacity = this.n * 0.25 + 0.25;
        // this.light.intensity = this.n * 4 + 4
    }
}
