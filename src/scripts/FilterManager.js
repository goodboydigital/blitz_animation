import * as PIXI from 'pixi.js';
import * as filters from 'pixi-filters';
import { TweenLite } from 'gsap';

export default class FilterManager
{
    constructor(container)
    {
        this.starContainer = container.starWarp;
        this.shockwaveFilter = new filters.ShockwaveFilter();
        this.shockwaveFilter.time = 3;

        this.bulgeShock = new filters.ShockwaveFilter();
        this.bulgeShock.time = 3;

        this.cornerBlur = new filters.ZoomBlurFilter();

        console.log(window);
        this.blurStrengthWarp = 0.5;
        this.blurStrengthIdle = 0.1;
        this.cornerBlur.center = [window.innerWidth / 2, window.innerHeight / 2];
        this.cornerBlur.innerRadius = window.innerWidth * 0.5;
        this.cornerBlur.radius = window.innerWidth;
        this.cornerBlur.strength = this.blurStrengthWarp;

        const options = {
            threshold: 0,
            bloomScale: 2,
            brightness: 2,
            blur: 1,
            quality: 5,
        };

        this.advanceBloom = new filters.AdvancedBloomFilter(options);
        // this.bulgePinch = new filters.BulgePinchFilter({radius: 50, strength: 0.3});

        this.starContainer.filterArea = new PIXI.Rectangle(0, 0, 1000000, 1000000);

        this.shockSin = 0;

        // this.coinContainer.filters = [this.cornerBlur];
    }

    update(delay)
    {
        this.advanceBloom.bloomScale -= (2) * 1 / 60;

        this.advanceBloom.bloomScale = Math.max(this.advanceBloom.bloomScale, -1);

        this.shockSin += 0.02;

        this.bulgeShock.time = (Math.sin(this.shockSin) * 0.5 + 0.5) * 0.3;
    }

    pulse()
    {
        this.shockwaveFilter.time = 0;
        TweenLite.to(this.shockwaveFilter, 1, { time: 3 });
        this.advanceBloom.bloomScale = 2;
        this.starContainer.filters = [this.shockwaveFilter, this.advanceBloom, this.bulgeShock, this.cornerBlur];
    }

    setBlurEffect(idle)
    {
        if (idle)
        {
            this.cornerBlur.strength = this.blurStrengthIdle;
        }
        else
        {
            this.cornerBlur.strength = this.blurStrengthWarp;
        }
    }

    removeEffects(target)
    {
        target.filters = [];
    }

    resize(resolution)
    {
        this.point = new PIXI.Point(resolution.width / 2, resolution.height / 2);

        this.cornerBlur.center = [resolution.width / 2, resolution.height / 2];

        this.shockwaveFilter.center = this.point;
        this.bulgeShock.center = this.point;
        // this.bulgePinch.center =point
    }

    lerp(v0, v1, t)
    {
        return v0 * (1 - t) + v1 * t;
    }
}
